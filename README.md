# node-vtech

## WORK IN PROGRESS

**This project is strictly for experimental purposes and should not be used in malicious ways.**

## Install

<< Not published to NPM yet >>

## Usage

*shell:*
```sh
# @TODO
```

*npm module*

```javascript
// @TODO
```

## Usage

Basically undocumented until the project is in a stable state.

### Checking if a CRN is available

```javascript
var account = new VTech({
  username: 'pid',
  password: 'password',
});

account.loginBanner().then(function() {
  return account.isCrnAvailable('0', '201501', '18952');
}).then(function(available) {
  console.log('is crn available?', available);
});
```
