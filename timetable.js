var querystring = require('querystring');

var when = require('when');
var cheerio = require('cheerio');

var R = require('ramda');

var util = require('./util');
var cheer = require('./cheer');

/*
   Querys the timetable with the provided options

   valid options:

   campus
   termyear
   core_code
   subj_code
   schdtype
   crse_number
   crn
   open_only
   disp_comments_in
   inst_name

   Output is a JS object containing the results
 */
exports.queryTimetableRaw = function(request, options) {

  options = options || {};

  if (!options.campus) { return when.reject(new Error('missing required option campus')); }
  if (!options.termyear) { return when.reject(new Error('missing required option termyear')); }

  var reqOptions = {
    hostname: 'banweb.banner.vt.edu',
    path: '/ssb/prod/HZSKVTSC.P_ProcRequest',
    method: 'POST',
    headers: {
      'Host': 'banweb.banner.vt.edu',
      'Connection': 'keep-alive',
      'Cache-Control': 'max-age=0',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
      'Origin': 'https://banweb.banner.vt.edu',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
      'Content-Type': 'application/x-www-form-urlencoded',
      'DNT': '1',
      'Referer': 'https://banweb.banner.vt.edu/ssb/prod/HZSKVTSC.P_ProcRequest',
      'Accept-Encoding': 'gzip, deflate',
      'Accept-Language': 'en-US,en;q=0.8'
    }
  };

  var reqBody = querystring.stringify({
    'CAMPUS': options.campus || '',
    'TERMYEAR': options.termyear || '',
    'CORE_CODE': options.core_code || 'AR%',
    'subj_code': options.subj_code || '%',
    'SCHDTYPE': options.schdtype || '%',
    'CRSE_NUMBER': options.crse_number || '',
    'crn': options.crn || '',
    'open_only': options.open_only || '',
    'disp_comments_in': options.disp_comments_in || 'N',
    'BTN_PRESSED': 'FIND class sections', // hardcoded - no other buttons needed for this
    'inst_name': options.inst_name || ''
  });

  return request.https(reqOptions, reqBody).then(function(res) {
    var $ = cheerio.load(res._body);

    // Clean up a given row and the values in it.
    var cleanRow = R.curry(function($, cells) {

      // insert filler columns after we run into (ARR) mess on timetable
      var fixArr = util.insertAfter('----- (ARR) -----', 'filler');

      // remove whitespace & \n from a row's columns
      var cleanText = R.compose(R.trim, R.replace(/\\n/g, ''));

      var clean =  R.compose(R.map(cleanText),
			     fixArr,
			     R.map(cheer.getText($)));

      return clean(cells);
    });

    var getEntryRows = function($, e) {
      return R.compose(
	R.map(cleanRow($)), // clean the row's cells
	R.map(cheer.getChildren($, 'td')), // get the row's cells
	cheer.getChildren($, 'tr') // get the row
      )(e);
    };

    var entryRows = getEntryRows($, $('table.dataentrytable'));

    return R.map(R.zipObj(R.head(entryRows)), R.tail(entryRows));
  });
};

exports.queryTimetable = function(request, options) {
  return exports.queryTimetableRaw(request, options).then(function(results) {

    return R.map(
      util.replaceKeys({
	'CRN ?': 'crn',
	Course: 'course',
	Title: 'title',
	'Type ?': 'type',
	'Cr Hrs': 'creditHours',
	Seats: 'seats',
	'Capacity\n ?': 'capacity',
	Instructor: 'instructor',
	'Days ?': 'days',
	Begin: 'begin',
	End: 'end',
	'Location ?': 'location',
	Exam: 'exam'
      }), results);

  });
};

exports.isCrnAvailable = function(request, campus, termyear, crn) {
  crn = '' + crn;
  return exports.queryTimetable(request, {
    campus: campus,
    termyear: termyear,
    crn: crn
  }).then(function(results) {
    if (results[0] && results[0].seats && results[0].seats.indexOf('Full') === -1) {
      return true;
    }

    return false;
  });
};
