



var querystring = require('querystring');

var when = require('when');
var cheerio = require('cheerio');

// login to VT-CAS
exports.cas = function(request, pid, password) {

  return request.https({
    hostname: 'auth.vt.edu',
    path: '/login',
    method: 'GET'
  }).then(function(res) {
    // pull out the necessary form variables to include in our login call
    var $ = cheerio.load(res._body);

    var lt = $('#login-form > fieldset > div.buttons > input[type="hidden"]:nth-child(1)').val();
    var execution = $('#login-form > fieldset > div.buttons > input[type="hidden"]:nth-child(2)').val();

    if (!lt || !execution) {
      return when.reject(new Error('missing lt or execution values'));
    }

    return request.https({
      hostname: 'auth.vt.edu',
      path: '/login',
      method: 'POST',
      headers: {
	'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
	'Accept-Encoding':'gzip, deflate',
	'Accept-Language':'en-US,en;q=0.8',
	'Cache-Control':'max-age=0',
	'Connection':'keep-alive',
	'Content-Type':'application/x-www-form-urlencoded',
	'DNT':'1',
	'Host':'auth.vt.edu',
	'Origin':'https://auth.vt.edu',
	'Referer':'https://auth.vt.edu/login',
	'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36'
      }
    }, querystring.stringify({
      username: pid,
      password: password,
      lt: lt,
      execution: execution,
      submit: '_submit',
      _eventId: 'submit'
    }));

  });
};

// Logs us into the banweb.banner.vt.edu site
exports.banner = function(request, pid, password) {
  // login with the central authententication system first
  return exports.cas(request, pid, password).then(function() {

    return request.https({
      hostname: 'banweb.banner.vt.edu',
      path: '/ssb/prod/twbkwbis.P_WWWLogin',
      method: 'GET',
      headers: {
	'Host': 'banweb.banner.vt.edu',
	'Connection': 'keep-alive',
	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
	'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
	'DNT': '1',
	'Accept-Encoding': 'gzip, deflate, sdch',
	'Accept-Language': 'en-US,en;q=0.8'
      }
    });

  }).then(function() {

    return request.https({
      hostname: 'banweb.banner.vt.edu',
      path: '/ssomanager_prod/c/SSB',
      method: 'GET',
      headers: {
	'Referer': 'https://banweb.banner.vt.edu/ssb/prod/twbkwbis.P_WWWLogin',
	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
	'Accept-Encoding': 'gzip, deflate, sdch',
	'Accept-Language': 'en-US,en;q=0.8',
	'Connection': 'keep-alive',
	'DNT': '1',
	'Host': 'banweb.banner.vt.edu',
	'Referer': 'https://banweb.banner.vt.edu/ssb/prod/twbkwbis.P_WWWLogin',
	'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36'
      }
    });
  });
};
