# vtech -- A shell script for interacting with Virginia Tech services.

## SYNOPSIS

vtech <sub-command> [flags]

## DESCRIPTION

...

## OPTIONS

### -h, --help
Markdown format of these man pages

### -u "PID", --username "PID"
Username/PID for a virginia tech account

### -p "PASSWORD", --password "PASSWORD"
Password for a virginia tech account

### --subargs "SUBCOMMAND ARGUMENTS"
A CSV string containing subcommand arguments

## AUTHORS
Trevor Senior <trevor@tsenior.com>

## BUGS
https://github.com/trevorsenior/node-vtech/issues
