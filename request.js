var when = require('when');

var urllib = require('url');
var https = require('https');
var querystring = require('querystring');

var tough = require('tough-cookie');

/*
   Wrapper around Node's https library that has cookie support
   while keeping it pretty raw. request library does a bit too
   much for us and mucks things up.


   Usage:

   var request = new Request();

   request.http(...);

   - - -

   by using it like this, we can have different sessions for every new
   request object
 */
module.exports = Request;
function Request(options, data) {

  var self = {};
  var cookiejar = new tough.CookieJar();

  self.https = function(options, data) {

    console.log('\n\n\n\n');
    console.log('>>> REQUEST');
    console.log(options.method + ': ' + options.hostname + options.path);

    options = options || {};
    options.headers = options.headers || {};

    return when.promise(function(resolve, reject) {
      // get the cookies for this hostname
      cookiejar.getCookieString('https://' + options.hostname, function(error, cookies) {
	return error ? reject(error) : resolve(cookies);
      });
    }).then(function(cookieString) {

      return when.promise(function(resolve, reject) {

	// set the cookies for this hostname
	if (cookieString) {
	  options.headers['Cookie'] = cookieString;
	}

	// set content-lenght if data is provided
	if (data) {
	  options.headers['Content-Length'] = data.length;
	}

	console.log('-> headers\n', options.headers);

	var req = https.request(options, function(res) {

	  res._req = req; // attach a reference back to the request

	  res.setEncoding('utf8');
	  var body = '';
	  res.on('error', function(error) { return reject(error); });
	  res.on('data', function(chunk) { body += chunk; });
	  res.on('end', function() {
	    res._body = body; // attach the response body to the object
	    return resolve(res);
	  });
	});

	if (data) {
	  console.log(querystring.parse(data));
	  console.log(data);
	  req.write(data);
	}
	req.end();
      });

    }).then(function(res) {

      console.log('\n>>> RESPONSE');
      console.log(res.headers);

      var setCookies = when.resolve();

      // Store any cookies
      var cookies = res.headers['set-cookie'];

      if (cookies) {
	setCookies = when.map(cookies, function(cookie) {
	  return when.promise(function(resolve, reject) {
	    cookiejar.setCookie(cookie, 'https://' + options.hostname, function(error) {
	      return error ? reject(error) : resolve();
	    });
	  });
	});
      }

      return setCookies.then(function() {

	// follow any redirects
	var location = res.headers['location'];

	if (location) {
	  var parsedLoc = urllib.parse(location);
	  options.headers['Host'] = parsedLoc.hostname;

	  return self.https({
	    hostname: parsedLoc.hostname,
	    path: parsedLoc.path,
	    method: 'GET',
	    headers: options.headers
	  });
	}

	// else return the response
	return res;
      });

    });

  };

  return self;
}
