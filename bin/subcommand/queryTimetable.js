
var VTech = require('../../vtech');

module.exports = function queryTimetable($) {

  if (!$('username') || !$('password')) {
    $.cerr('missing username or password').exit(1);
  }

  var account = new VTech({
    username: $('username'),
    password: $('password'),
  });

  return account.loginBanner().then(function() {
    return account.isCrnAvailable('0', '201501', '18952');
  }).then(function(available) {
    console.log('is crn available?', available);
  });
}
