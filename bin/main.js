#!/usr/bin/env node

var path = require('path');
var fs = require('fs');

var when = require('when');
var main = require('main');

/*
 */
var man = path.join(__dirname, '..', 'man', 'vtech.1.md');
main.run(module, man, function($) {

  if ($('h') || $('help')) {
    console.log(fs.readFileSync(man));
    process.exit();
  }

  if ($.pos.length < 1) {
    console.error('missing required arguments');
    process.exit();
  }

  var promise;

  switch($(0)) {
    case 'query-timetable': promise = require('./subcommand/queryTimetable')($);
  }

  if (!promise) {
    promise = when.reject(new Error('invalid subcommand'));
  }

  return promise.done();

});
