
var when = require('when');

var Request = require('./request');
var login = require('./login');
var timetable = require('./timetable');

module.exports = function VTech(config) {
  var self = {};

  // create a unique request instance for this VTech instance
  self._request = new Request();

  self._username = config.username || '';
  self._password = config.password || '';

  self.loginCas = function() {
    return login.cas(self._request, self._username, self._password);
  };

  self.loginBanner = function() {
    return login.banner(self._request, self._username, self._password);
  };

  // @TODO
  self.logout = function() {};

  /* 
  timetable
   */
  self.queryTimetable = function(options) {
    return timetable.queryTimetable(self._request, options);
  };

  self.isCrnAvailable = function(campus, termyear, crn) {
    return timetable.isCrnAvailable(self._request, campus, termyear, crn);
  };
  

  return self;
}

