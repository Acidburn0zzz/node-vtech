
var R = require('ramda');

// seeks through an array, finds a given element value, then inserts
// the value given directly after it. Returns a new array with the
// adjustments
exports.insertAfter = R.curry(function(find, insertAfter, array) {

  function process(array, startAt) {

    startAt = startAt || 0;

    var arr = array.slice();
    var len = arr.length;
    var i = startAt;

    for (; i < len; ++i) {
      // if we find the element that matches the given `find` object
      if (R.eq(find, arr[i])) {
	// insert the new element after this one, and call process again
	// at the index that we want to continue with
	return process(R.insert(i + 1, insertAfter, arr), i + 2);
      }
    }

    return arr;
  }

  return process(array);
});


// replace a key in an object
exports.replaceKey = R.curry(function(oldKey, newKey, obj) {
  if (obj[oldKey]) {
    obj[newKey] = obj[oldKey];
    delete obj[oldKey];
  }
  return obj;
});


exports.replaceKeys = R.curry(function(keyMap, obj) {
  function replace(oldKey) {
     return exports.replaceKey(oldKey, keyMap[oldKey], obj);
  }

  var keys = R.keys(obj);
  keys.forEach(replace);
  return obj;
});
