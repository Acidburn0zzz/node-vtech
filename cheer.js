//
// Functional wrappers for the cheerio library
//
// All expect the cheerio object as the first parameter
//

var R = require('ramda');

// get the text of a cheerio dom element
var getText = exports.getText = R.curry(function($, e) {
  return $(e).text();
});

// remove a child selector from a cheerio element
var removeChild = exports.removeChild = R.curry(function($, removeSelector, e) {
  $(e).find(removeSelector).remove();
  return $(e);
});

// gets the children array from a cheerio element
var getChildren = exports.getChildren = R.curry(function($, childname, e) {
  return $(e).children(childname).get();
});


/*
// gets the text from the children array from a cheerio element
// usage: getCildrenText($, childname, e)
var getChildrenText = exports.getChildrenText = R.compose(
  R.map(getText),
  getChildren);

// Get the children, remove elements from them, and then return the text
// value
// usage: getChildrenTextRemove($, childname, removeSelector, e);
var getChildrenTe = exports.getChildrenTextRemove =
R.curry(function($, childnrme, removeSelector, e) {
  return  R.compose(
    R.map(getText
    R.map(removeChild($, removeSelector)
      getChildren);
});
*/
